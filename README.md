# NodeAlpine APP On 3000
## DEUTSCH:

Das Docker-Image basiert auf dem offiziellen node:14-alpine-Image und enthält alle Abhängigkeiten und Konfigurationen, die zum Ausführen der Anwendung erforderlich sind.

Das Dockerfile enthält die folgenden Schritte:

+ Setzen des Arbeitsverzeichnisses auf /app
*  Kopieren der Anwendungsdateien in das Arbeitsverzeichnis
- Installieren der Abhängigkeiten mit npm install
- Ausführen der Tests mit npm test
- Bauen der Anwendung mit npm run build
- Öffnen des Ports 8080
- Starten der Anwendung mit dem Befehl npm start

### Öffne eine Terminal- oder Kommandozeile und führe den folgenden Befehl aus, um das Docker-Image zu erstellen:


```CLI
docker build -t migzzenzei/repository .
```

#### Dieser Befehl verwendet das Dockerfile in deinem aktuellen Verzeichnis (daher der Punkt am Ende) und erstellt ein Image mit dem Tag "migzzenzei/repository".

### Sobald Image erstellt, führe es aus, mit dem folgenden Befehl:

```CLI
docker run -p 3000:3000 migzzenzei/repository
```

#### Dieser Befehl startet einen Docker-Container mit dem Image "migzzenzei/repository" und leitet den Port 3000 von deinem lokalen System zum Port 3000 im Container weiter. 
Die Anwendung sollte nun unter http://localhost:3000 erreichbar sein.

Das Dockerfile, verwendet das Basisimage "node:19-alpine", setzt das Arbeitsverzeichnis auf "/src", kopiert die Anwendungsdateien in das Verzeichnis, installiert die Abhängigkeiten, baut die Anwendung und startet sie schließlich mit dem Befehl 

```CLI
npm run start
``` 

Der Port 3000 wird auch für den Container geöffnet.

##ENGLISH:
The Docker image is based on the official node:14-alpine image and includes all dependencies and configurations needed to run the application.

The Dockerfile contains the following steps:

- Set working directory to /app
- Copy application files to working directory
- Install dependencies with npm install
- Run the tests with npm test
- Build the application with npm run build
- Open port 8080
- Start the application with the npm start command

### Open a terminal or command line and run the following command to build the Docker image:


```CLI
docker build -t migzzenzei/repository .
```

#### This command takes the Dockerfile in your current directory (hence the dot at the end) and builds an image tagged "migzzenzei/repository".

### Once image is created, run it with the following command:

```CLI
docker run -p 3000:3000 migzzenzei/repository
```

#### This command starts a Docker container with the image "migzzenzei/repository" and forwards port 3000 from your local system to port 3000 in the container.
The application should now be accessible at http://localhost:3000.

The Dockerfile, uses the base image "node:19-alpine", sets the working directory to "/src", copies the application files into the directory, installs the dependencies, builds the application and finally starts it with the command

```CLI
npm run start
```

Port 3000 is also opened for the container.
